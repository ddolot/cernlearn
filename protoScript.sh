protoc chat.proto cernlearn.proto --proto_path=api/proto/v1 --proto_path=. --go_out=plugins=grpc:cernlearn-srv/pkg/api/v1
protoc chat.proto cernlearn.proto --proto_path=api/proto/v1 --proto_path=third_party  --dart_out=grpc:cern_learn/lib/api/v1
cd cernlearn-srv/pkg/cmd/server
CGO_ENABLED=0 GOOS=linux go build
cd -
