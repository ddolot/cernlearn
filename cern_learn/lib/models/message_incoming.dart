import 'package:meta/meta.dart';

import 'message.dart';

/// MessageIncoming is class defining incoming message data (id and text)
class MessageIncoming extends Message {
  /// Constructor
  final String pFrom;

  MessageIncoming({String id, String pFrom, @required String text})
      : this.pFrom=pFrom, super(id: id, text: text);

  MessageIncoming.copy(MessageIncoming original)
      : this(id: original.id, pFrom: original.pFrom, text: original.text);
}
