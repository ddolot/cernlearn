class Post{
  String subject;
  String topic;
  String preferedTime;
  String postedBy;
  String message;

  Post(this.subject, this.topic, this.preferedTime, this.postedBy, this.message);
  Post.empty();
}