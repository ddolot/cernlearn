import 'package:cern_learn/pages/AddIssue.dart';
import 'package:cern_learn/pages/NewsFeed.dart';
import 'package:cern_learn/theme.dart';
import 'package:cern_learn/utils/MainDrawer.dart';
import 'package:flutter/material.dart';
import 'package:cern_learn/utils/Constants.dart';
import 'package:cern_learn/api/chat_service.dart';

import 'package:cern_learn/blocs/application_bloc.dart';
import 'package:cern_learn/blocs/bloc_provider.dart';
import 'package:cern_learn/blocs/message_events.dart';

import 'package:cern_learn/pages/chat_screen.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';

import 'package:grpc/grpc.dart';
import 'package:cern_learn/api/v1/cernlearn.pbgrpc.dart' as grpc;

import 'package:cern_learn/utils/globals.dart' as globals;

import 'dart:math';


//void main() => runApp(MyApp());

/// main is entry point of Flutter application
void main() {
  var rng = new Random();

  return runApp(BlocProvider<ApplicationBloc>(
    bloc: ApplicationBloc(),
    child: App(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: lightColor,
        primaryTextTheme: TextTheme(
          title: TextStyle(
            color: Colors.white
          )
        )
      ),
      home: LoginPage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: MainDrawer(),
      body: NewsFeed(),
      floatingActionButton: FloatingActionButton(
        onPressed: pushAddIssuePage,
        child: Icon(Icons.add),
        foregroundColor: Colors.white,
        backgroundColor: darkColor
      ),
    );
  }


  void pushAddIssuePage() {
    Navigator.push(context,
        MaterialPageRoute(
            builder: (context) => AddIssue()
        ));
  }
}

class AuthorizationPage extends StatefulWidget {
  AuthorizationPage({Key key, this.title}) : super(key: key);

  final String title;
  
  @override
  _AuthorizationPageState createState() => _AuthorizationPageState();
}

void setupToken(String token) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString("cernlearn.token", token);
}

class _AuthorizationPageState extends State<AuthorizationPage> {

  final webview = FlutterWebviewPlugin();
  BuildContext _webViewContext;

  @override
  void initState() {
    super.initState();
    webview.onUrlChanged.listen((str) {
      if (str.contains('https://test-cernlearn.web.cern.ch/test-cernlearn')) {
        String code = Uri.parse(str).queryParameters["code"];
        setupToken(code);

        ClientChannel client = ClientChannel(
          serverIP, // Your IP here or localhost
          port: serverPort,
          options: ChannelOptions(
            //TODO: Change to secure with server certificates
            credentials: ChannelCredentials.insecure(),
            idleTimeout: Duration(seconds: 1),
          ),
        );


        grpc.ServiceRequest request =  grpc.ServiceRequest.create();
        request.api = "v1";
        request.token = code;
        grpc.LoginServiceClient(client).loginWithCERN(request).then((grpc.ServiceResponse response){
          if(response.status == grpc.Status.OK) {
            Navigator.pop(_webViewContext);

            globals.currentName = response.message;
            Navigator.push(_webViewContext,
                MaterialPageRoute(
                    builder: (context) => MyHomePage(title: "CERNLearn Hub")
                ));
          }
        });
      }
    });
  }

  void getName(String code) {

  }

  @override
  Widget build(BuildContext context) {
    _webViewContext = context;
    return WebviewScaffold(
        url: "https://oauth.web.cern.ch/OAuth/Authorize?scope=read:user&response_type=code&redirect_uri=https%3A%2F%2Fcern.ch%2Ftest-cernlearn&client_id=cernlearn",

    );
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              //padding: EdgeInsets.symmetric(horizontal: 24.0),
              children: <Widget>[
                SizedBox(height: 80.0),
                Column(
                  children: <Widget>[
                    SizedBox(height: 135.0),
                    Text('Welcome to')
                  ],
                ),
                DrawerHeader(
                  decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(Constants.logoTransparent), fit: BoxFit.fitWidth)
                  ),
                  child: null
                  ,
                ),
                Container(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      onPressed: () {
                        handleThePress(context);
                        //this.dispose();
                      },
                      padding: EdgeInsets.all(10),
                      color: Colors.cyan,
                      child: Text('Log in', style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
                    )
                    ,
                    margin: EdgeInsets.symmetric(horizontal: 130)
                ),
                Expanded(
                  child: Align (
                    alignment: Alignment.bottomCenter,
                    child: 
                    Padding (
                      padding: EdgeInsets.all(8.0),
                      child: new Text("by Jacek, Kang Ying, Michal,\nNicole, Olivier, Steffen, Usamah",
                        textAlign: TextAlign.center,),
                    )
                  ),
                )
              ],
            )
        )
    );
  }
}

Future<Null> handleThePress(BuildContext context) async {
  //Uri uri = await getInitialUri();
  //if(uri == null){
    Navigator.push(context,
        MaterialPageRoute(
            builder: (context) => AuthorizationPage()
        ));

  /*} else {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("cernlearn.token", uri.queryParameters["code"]);
*/

}

// Stateful application widget
class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AppState();
}

// State for application widget
class _AppState extends State<App> {
  // BLoc for application
  ApplicationBloc _appBloc;

  /// Chat client service
  ChatService _service;

  bool _isInit = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // As the context of not yet available at initState() level,
    // if not yet initialized, we get application BLoc to start
    // gRPC isolates
    if (_isInit == false) {
      _appBloc = BlocProvider.of<ApplicationBloc>(context);

      // initialize Chat client service
      _service = ChatService(
          onMessageSent: _onMessageSent,
          onMessageSendFailed: _onMessageSendFailed,
          onMessageReceived: _onMessageReceived,
          onMessageReceiveFailed: _onMessageReceiveFailed);
      _service.start();

      _listenMessagesToSend();

      if (mounted) {
        setState(() {
          _isInit = true;
        });
      }
    }
  }

  void _listenMessagesToSend() async {
    await for (var event in _appBloc.outMessageSend) {
      _service.send(event.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CERNLearn',
      theme: ThemeData(
        primarySwatch: lightColor,
          primaryTextTheme: TextTheme(
              title: TextStyle(
                  color: Colors.white
              )
          ),
        primaryIconTheme: IconThemeData(
          color: Colors.white
        ),
      ),
      home: LoginPage(),
    );
  }

  @override
  void dispose() {
    // close Chat client service
    _service.shutdown();
    _service = null;

    super.dispose();
  }

  /// 'outgoing message sent to the server' event
  void _onMessageSent(MessageSentEvent event) {
    debugPrint('Message "${event.id}" sent to the server');
    _appBloc.inMessageSent.add(event);
  }

  /// 'failed to send message' event
  void _onMessageSendFailed(MessageSendFailedEvent event) {
    debugPrint(
        'Failed to send message "${event.id}" to the server: ${event.error}');
    _appBloc.inMessageSendFailed.add(event);
  }

  /// 'new incoming message received from the server' event
  void _onMessageReceived(MessageReceivedEvent event) {
    debugPrint('Message received from the server: ${event.text}');
    _appBloc.inMessageReceived.add(event);
  }

  /// 'failed to receive messages' event
  void _onMessageReceiveFailed(MessageReceiveFailedEvent event) {
    debugPrint('Failed to receive messages from the server: ${event.error}');
  }
}

