import 'package:cern_learn/main.dart';
import 'package:cern_learn/pages/TopicList.dart';
import 'package:cern_learn/utils/Constants.dart';
import 'package:flutter/material.dart';
import 'package:cern_learn/pages/chat_screen.dart';

import 'package:cern_learn/utils/globals.dart' as globals;


/// Widget representing a drawer that is used throughout the whole application.
class MainDrawer extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage(Constants.logoTransparent), fit: BoxFit.fitWidth)
            ),
            child: null
            ,
          ),
          ListTile(
          title: Text("Hello, " + globals.currentName + "!", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600))
          ),
          ListTile(
          title: Text('Homepage', style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400)),
            onTap: () {
            Navigator.push(context,
            MaterialPageRoute(
            builder: (context) => MyHomePage(title: "CERNLearn Homepage")
            ));
            },
    ),
          ListTile(
            title: Text('Chat', style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400)),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => ScreenPage()
                  ));
            },
          ),
          ListTile(
              title: Text('Topics', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400)),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => TopicList()
                  ));
            },
          ),
          ListTile(
              title: Text("Settings", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400))
          ),
          ListTile(
              title: Text("Log out", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400))
          ),
        ],
      ),
    );

  }
}