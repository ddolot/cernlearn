class Constants{
  static String logo = "images/logo.png";
  static String logoTransparent = "images/logo_transparent.png";

  static List<String> topics = ['Hackathon', 'Cooking','Game Dev','Photography','Astrophysics', 'Biology', 'Chemistry', 'Computer Science', 'Computer Security',
    'Dance', 'Experimental Physics', 'Food and Drinks', 'Games', 'Hiking',
    'Literature', 'Mathematics', 'Music', 'Painting', 'Particle Physics', 'Physics', 'Switzerland', 'Sports', 'Travel'];
}