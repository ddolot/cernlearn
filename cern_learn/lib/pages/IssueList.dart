import 'package:cern_learn/models/Post.dart';
import 'package:cern_learn/pages/AddIssue.dart';
import 'package:cern_learn/pages/MessageScreen.dart';
import 'package:cern_learn/theme.dart';
import 'package:cern_learn/utils/Constants.dart';
import 'package:cern_learn/utils/MainDrawer.dart';
import 'package:flutter/material.dart';
import 'dart:io' as io;

import 'package:shared_preferences/shared_preferences.dart';

class IssueList extends StatefulWidget{
  
  String topic;
  IssueList(this.topic);
  
  @override
  IssueListState createState() => IssueListState(topic);
}

class IssueListState extends State<IssueList>{
  String topic;
  IssueListState(this.topic);
  bool iconPressed = false;
  var posts;
  bool showRequests = false;
  var str = "Showing offers";

  @override
  Widget build(BuildContext context) {

    if (showRequests == true){
      posts = getDumbRequests();
    }
    else {
      posts = getDumbOffers();
    }

    var notificationIcon = Icon(iconPressed ? Icons.notifications_active : Icons.notifications);
    return Scaffold(
      appBar: AppBar(
        title: Text("See issues in $topic"),
        actions: <Widget>[
          // Notify Button
          IconButton(
            icon: notificationIcon,
            onPressed: () {
              setState(() {
                iconPressed = !iconPressed;
              });
            },
          )
        ],
      ),
      drawer: MainDrawer(),
      floatingActionButton: FloatingActionButton(
        onPressed: pushAddIssuePage,
        child: Icon(Icons.add),
        foregroundColor: Colors.white,
        backgroundColor: darkColor,
      ),
    body: Column(
        children: <Widget>[
          SwitchListTile(
//            inactiveThumbColor: darkColor,
            inactiveTrackColor: darkColor,
            inactiveThumbColor: darkColor,
            title: Text(str, style: TextStyle(color: Colors.black, fontFamily: 'Poppins',
                fontWeight: FontWeight.w400,
                fontSize: 18.0)),
            value: showRequests,
            onChanged:(bool value) {
              setState((){
                showRequests = value;
              });
              var newPosts;
              if (showRequests == true){
                newPosts = getDumbRequests();
                setState(() {
                  str = "Showing requests";
                });
              }
              else{
                newPosts = getDumbOffers();
                setState(() {
                  str = "Showing offers";
                });
              }
              print("New posts: " + newPosts.toString());
              setState(() => {
                posts = newPosts
              });

            },)
          ,Flexible(
          child: new Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 30, bottom: 10),
            color: Colors.white,
            child: new ListView.builder(
              //itemExtent: 300.0, // THIS IS THE HEIGHT OF THE ITEM!!!!
              shrinkWrap: true,
              itemCount: posts.length,
              itemBuilder: (_, index) => new TopicRow(posts[index]),
            ),
          ),
        )])
    );
  }


  void pushAddIssuePage() {
    Navigator.push(context,
        MaterialPageRoute(
            builder: (context) => AddIssue()
        ));
  }

  List<Post> getDumbRequests(){
    var posts = List<Post>();

    posts.add(Post("I need help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));
    posts.add(Post("I need help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));
    posts.add(Post("I need help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));
    posts.add(Post("I need help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));
    posts.add(Post("I need help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));


    return posts;
  }

  List<Post> getDumbOffers(){
    var posts = List<Post>();

    posts.add(Post("I can provide help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));
    posts.add(Post("I can provide help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));
    posts.add(Post("I can provide help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));
    posts.add(Post("I can provide help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));
    posts.add(Post("I can provide help in $topic.", topic, "Lunch", "Nicole", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus lectus, semper eu ligula vel, congue rutrum nisi. Praesent eu."));


    return posts;
  }
}

class TopicRow extends StatelessWidget{
  final Post post;

  TopicRow(this.post);

  @override
  Widget build(BuildContext context) {
    final planetThumbnail = new Container(
        alignment: new FractionalOffset(0.0, 0.5),
        margin: const EdgeInsets.only(top: 10.0),
        child: new Container(
          width: 50.0,
          height: 50.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            image: new DecorationImage(
                fit: BoxFit.fill,
                image:  AssetImage("images/${this.post.topic}.png")
            ),
          ),
        )
    );

    final planetCard = new Container(
      width: 550,
        height: 140,
        margin: const EdgeInsets.only(left: 3.0, right: 3.0),
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: new BorderRadius.circular(10.0),
          color: lightColor
        ),
        child: Container(
            height: 100,
            margin: const EdgeInsets.only(top: 16.0, left: 45.0, right: 10),
            constraints: new BoxConstraints.expand(),
            child: new Column(

              children: <Widget>[Align(alignment: Alignment.topLeft,child:Text(this.post.subject, style: TextStyle(color: Colors.white,
                  fontSize: 18.0, fontWeight: FontWeight.w400))
              ),
                SizedBox(height:5),
                Align(alignment: Alignment.topLeft,child:Text("posted by "+ this.post.postedBy, style: TextStyle(color: Colors.white,
                    fontSize: 10.0)
                )),
                SizedBox(height:5),
                Text(this.post.message, style: TextStyle(color: Colors.white,
                    fontSize: 14.0, fontWeight: FontWeight.w400)
                )
              ],
            )
        )
    );

    return new Container(
//        height: 100.0,
      margin: const EdgeInsets.only(top: 10.0, bottom: 8.0),
      child: new FlatButton(
        onPressed: () => {
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (context) => MessageScreen(post)
              ))
        },
        child: new Stack(
          children: <Widget>[
            planetCard,
            planetThumbnail,
          ],
        ),
      ),
    );
  }


}


