import 'package:cern_learn/models/Post.dart';
import 'package:cern_learn/pages/MessageScreen.dart';
import 'package:cern_learn/theme.dart';
import 'package:cern_learn/utils/Constants.dart';
import 'package:cern_learn/utils/MainDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io' as io;

class NewsFeed extends StatefulWidget{
  @override
  NewsFeedState createState() => NewsFeedState();
}

class NewsFeedState extends State<NewsFeed>{

  var posts = getDumbRequests();
  bool showRequests = false;
  var str = "Showing offers";


  @override
  Widget build(BuildContext context) {


    if (showRequests == true){
      posts = getDumbRequests();
    }
    else {
      posts = getDumbOffers();
    }

    return Column(

        children: <Widget>[
          new Container(
//            color: Colors.white,
          child:SwitchListTile(
//            inactiveThumbColor: darkColor,
            inactiveTrackColor: darkColor,
            inactiveThumbColor: darkColor,
            title: Text(str, style: TextStyle(color: Colors.black, fontFamily: 'Poppins',
        fontWeight: FontWeight.w400,
        fontSize: 18.0)),
            value: showRequests,
            onChanged:(bool value) {
              setState((){
                showRequests = value;
              });
              var newPosts;
              if (showRequests == false){
                newPosts = getDumbOffers();
                setState(() {
                  str = "Showing offers";
                });
              }
              else{
                newPosts = getDumbRequests();
                setState(() {
                  str = "Showing requests";
                });
              }
              print("New posts: " + newPosts.toString());
              setState(() => {
                posts = newPosts
              });

              },)
            ,
    ),
          Flexible(
          child: new Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 30, bottom: 10),
            decoration: BoxDecoration(
//              color: Colors.white
            ),
            child: new ListView.builder(
              //itemExtent: 300.0, // THIS IS THE HEIGHT OF THE ITEM!!!!
              shrinkWrap: true,
              itemCount: posts.length,
              itemBuilder: (_, index) => new TopicRow(posts[index]),
            ),
          ),
        )]);
  }

}

class TopicRow extends StatelessWidget{
  final Post post;

  TopicRow(this.post);

  @override
  Widget build(BuildContext context) {
    final planetThumbnail = new Container(
        alignment: new FractionalOffset(0.0, 0.5),
          margin: const EdgeInsets.only(top: 15.0),
            child: new Container(
              width: 50.0,
              height: 50.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                image: new DecorationImage(
                    fit: BoxFit.fill,
                    image:  AssetImage("images/${this.post.topic}.png")
                ),
              ),
            )
    );

    final planetCard = new Container(
        height: 140,
        margin: const EdgeInsets.only(left: 30.0, right: 5.0),
        decoration: new BoxDecoration(
          //color: Colors.lightGreen,
          shape: BoxShape.rectangle,
          borderRadius: new BorderRadius.circular(20.0),
        ),
        child: Container(
            height: 100,
            margin: const EdgeInsets.only(top: 16.0, left: 30.0, right: 18),
            constraints: new BoxConstraints.expand(),
            child: new Column(

              children: <Widget>[
                Align(alignment: Alignment.topLeft, child:Text(
                    this.post.subject, style: TextStyle(color: Colors.white,
                    fontSize: 18.0, fontWeight: FontWeight.w400)
                )),
                SizedBox(height:5),
                Text(this.post.message, style: TextStyle(color: Colors.white,
                    fontSize: 14.0, fontWeight: FontWeight.w400)
                ),
                SizedBox(height:5),
                Align(alignment: Alignment.topRight, child:Text(
                    "posted by "+ this.post.postedBy, style: TextStyle(color: Colors.white,
                    fontSize: 10.0)
                )),
              ],
            )
        )
    );

    return new Container(
//        height: 100.0,
    color: lightColor,
      margin: const EdgeInsets.only(top: 10.0, bottom: 8.0),
      child: new FlatButton(
        onPressed: () => {
        Navigator.push(context,
        MaterialPageRoute(
        builder: (context) => MessageScreen(this.post)
        ))
        },

        child: new Stack(
          children: <Widget>[
            planetCard,
            planetThumbnail,
          ],
        ),
      ),
    );
  }

}

List<Post> getDumbRequests(){
  var posts = List<Post>();

  posts.add(Post("Game Dev", "Game Dev", "Lunch", "Nicole, 1h ago", "I'm a passionate gamer and would like to start developing the COMPASS shift simulator..."));
  posts.add(Post("Cooking", "Cooking", "Lunch", "Steffen, 1d ago", "Hey, I'm looking for someone to help me understand the molecular cuisine..."));
  posts.add(Post("Photography", "Photography", "Lunch", "Jacek, 2d ago", "Hi! I'm new to photography and would like to know how to approach macro shots the best..."));
  posts.add(Post("AI", "AI", "Lunch", "Usamah, 4d ago", "Dear all, I'm concerned about ethics in the latest AI development and would like to speak to an expert to get a better idea of the whole concept..."));

  return posts;
}

List<Post> getDumbOffers(){

  var posts = List<Post>();
  posts.add(Post("Offering help with RNNS", "Computer Science", "Dinner", "Steffen", "I am willing to offer some help for someone who wants to take up RNNS."));
  posts.add(Post("Offering help with guitar", "Music", "Anytime", "Jacek", "I am willing to offer some help for someone who wants to take up guitar."));

  return posts;
}
