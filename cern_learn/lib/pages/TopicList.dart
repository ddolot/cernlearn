import 'package:cern_learn/pages/IssueList.dart';
import 'package:cern_learn/theme.dart';
import 'package:cern_learn/utils/Constants.dart';
import 'package:cern_learn/utils/MainDrawer.dart';
import 'package:flutter/material.dart';
import 'dart:io' as io;

class TopicList extends StatefulWidget{
  @override
  TopicListState createState() => TopicListState();
}

class TopicListState extends State<TopicList>{

  var topics = Constants.topics;
  String _searchText = "";
  var _filtered_topics = new List();
  var _searchIcon = Icon(Icons.search);
  Widget _appBarTitle = new Text("All Topics");
  final TextEditingController _filter = new TextEditingController();

  TopicListState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          this._searchText = "";
          this._filtered_topics = this.topics;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });

    _filtered_topics = topics;
  }

  Widget _buildList() {
    if (_searchText.isNotEmpty) {
      List tempList = new List();
      for (int i = 0; i < _filtered_topics.length; i++) {
        if (_filtered_topics[i].toLowerCase().contains(_searchText.toLowerCase())) {
          tempList.add(_filtered_topics[i]);
        }
      }
      _filtered_topics = tempList;
    }

    return ListView.builder(
      itemExtent: 110,
      itemCount: topics == null ? 0 : _filtered_topics.length,
      itemBuilder: (_, index) => new TopicRow(_filtered_topics[index]),
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          autofocus: true,
          controller: _filter,
          decoration: new InputDecoration(
            prefixIcon: new Icon(Icons.search),
            hintText: 'Search...'
          ),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('All Topics');
        _filtered_topics = topics;
        _filter.clear();
      }
    });
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
}

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: _buildBar(context),
      drawer: MainDrawer(),
      body: Column(
        children: <Widget>[Flexible(
        child: new Container(
        color: Colors.white,
          child: _buildList(),
        ),
      )]
      ));

  }

}

class TopicRow extends StatelessWidget{
  final String topic;

  TopicRow(this.topic);

    @override
    Widget build(BuildContext context) {
      final planetThumbnail = new Container(
          alignment: new FractionalOffset(0.0, 0.5),
         margin: const EdgeInsets.only(left: 55.0),
              child: new Container(
                width: 50.0,
                height: 50.0,
                decoration: new BoxDecoration(
                  //shape: BoxShape.circle,
                  image: new DecorationImage(
                    fit: BoxFit.fill,
                    image:  AssetImage("images/${this.topic}.png")
                  ),
                ),
              )
      );

      final planetCard = new Container(
        height: 140,
        margin: const EdgeInsets.only(left: 20.0, right: 20.0),
        decoration: new BoxDecoration(
          color: lightColor,
          shape: BoxShape.rectangle,
          borderRadius: new BorderRadius.circular(30.0),
        ),
        child: Container(
          height: 100,
          margin: const EdgeInsets.only(top: 25.0, left: 30.0, right: 18),
          constraints: new BoxConstraints.expand(),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[Text(topic, style: TextStyle(color: Colors.white, fontFamily: 'Poppins',
              fontWeight: FontWeight.w600,
              fontSize: 18.0)
        )],
      )
      )
      );

      return new Container(
//        height: 100.0,
        margin: const EdgeInsets.only(top: 10.0, bottom: 8.0),
        child: new FlatButton(
          onPressed: () => {
        Navigator.push(context,
            MaterialPageRoute(
                builder: (context) => IssueList(topic)
            ))
      },

          child: new Stack(
            children: <Widget>[
              planetCard,
              planetThumbnail,
            ],
          ),
        ),
      );
    }

}