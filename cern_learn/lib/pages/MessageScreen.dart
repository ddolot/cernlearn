import 'package:cern_learn/models/Post.dart';
import 'package:cern_learn/pages/chat_screen.dart';
import 'package:cern_learn/theme.dart';
import 'package:cern_learn/utils/MainDrawer.dart';
import 'package:flutter/material.dart';

class MessageScreen extends StatefulWidget{

  Post post;

  @override
  State<StatefulWidget> createState() => MessageScreenState(post);

  MessageScreen(this.post);

}

class MessageScreenState extends State<MessageScreen>{

  Post post;
  MessageScreenState(this.post);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(post.subject),
      ),
      drawer: MainDrawer(),
      body: buildMessageBody()
    );
  }

  Widget buildMessageBody(){
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(left: 15, top: 15, right: 15),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Card(
              child: Text(post.subject, style: TextStyle(color: darkColor, fontFamily: 'Poppins',
            fontWeight: FontWeight.w600,
            fontSize: 20.0)
              )
        ),
            SizedBox(height: 20),
            Card(
              child: Text('posted by ' +post.postedBy, style: TextStyle(color: darkColor, fontFamily: 'Poppins',
              fontWeight: FontWeight.w600,
              fontSize: 16.0)
              )
            ),
              Card(
                  child: Text(post.message, style: TextStyle(color: Colors.black, fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0)
                  )
              ),
              Card(
                margin: EdgeInsets.only(left:110, right:5),
                child:  Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                    child: Row(
                      children: <Widget>[
                      Image( fit: BoxFit.fitWidth ,image: AssetImage("images/Lightbulb.png"),width: 60,),
                      Text("I am interested!", style: TextStyle(color: darkColor, fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0)),
                    ]
                      ), onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(
                                builder: (context) => ScreenPage()
                            ));
                    },
    )
                  ]
                    )

                  )
                ])
              );
  }

}