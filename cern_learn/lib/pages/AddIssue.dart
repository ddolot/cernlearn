import 'package:cern_learn/main.dart';
import 'package:cern_learn/theme.dart';
import 'package:cern_learn/utils/Constants.dart';
import 'package:cern_learn/utils/MainDrawer.dart';
import 'package:flutter/material.dart';

class AddIssue extends StatefulWidget{
  

  AddIssue();

  @override
  AddIssueState createState() => AddIssueState();

}

class AddIssueState extends State<AddIssue>{
  

  String topicValue = "Hackathon"; // i.e. the topic
  String timeValue = "Coffee time";
  var subjectController = TextEditingController();
  var messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    this.subjectController.text = 'Webfest';
    this.messageController.text = 'I attended the CERN Webfest 2019 and had so much fun. We developed the software you are currently using. The expertise I can offer is Flutter, Dart and a chat backend with CERN SSO. Please feel free to contact me for a coffee meeting. ';
    return Scaffold(
      appBar: AppBar(
        title: Text('Post expertise'),
      ),
      body: buildForm(),
      drawer: MainDrawer(),
    );

  }

  AddIssueState();


  Widget buildForm() {

    var subject = Container(
      child: Card(
          child: TextFormField(
            controller: subjectController,
            autofocus: false,
            maxLines: 1,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
            ),

          )
      ),
      margin: EdgeInsets.symmetric(horizontal: 10),

    );

    var hint = Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
      child: Text("Choose the topic", style: TextStyle(color: lightColor))
    );

    var topic = Container(
      width: 230.0,
      margin: EdgeInsets.symmetric(horizontal: 10),

      child: DropdownButtonHideUnderline(
      child: ButtonTheme(
        alignedDropdown: true,
          child: DropdownButton<String>(
        value: topicValue,
        onChanged: (String newValue) {
        setState(() {
          topicValue = newValue;
        });
        },
        items: Constants.topics.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      )
      )
      ),
    );

    var meetingTimeHint = Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
      child: Text("Choose your preferred meeting time", style: TextStyle(color: lightColor))
    );

    var preferedMeetingTime = Container(
      width: 230.0,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: DropdownButtonHideUnderline(
          child: ButtonTheme(
              buttonColor: Colors.white,
              alignedDropdown: true,
              child: DropdownButton<String>(
                value: timeValue,
                onChanged: (String newValue) {
                  setState(() {
                    timeValue = newValue;
                  });
                },
                items: <String>["Coffee time", "Lunch", "Dinner", "Anytime"].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              )
          )
      ),
    );

    var text = Container(
      child: Card(
        child: TextFormField(
          controller: messageController,
          autofocus: false,
          maxLines: 10,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(6),
            border: InputBorder.none,
          ),
        ),
      ),
      margin: EdgeInsets.symmetric(horizontal: 10),
    );

    var addButton = Container(
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32),
          ),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(
                    builder: (context) => MyHomePage(title: "CERNLearn Hub")
                ));
          },
          padding: EdgeInsets.all(20),
          color: Colors.cyan,
          child: Text('Post', style: TextStyle(color: Colors.white),
          ),
        )
        ,
        margin: EdgeInsets.symmetric(horizontal: 140)
    );

    var gap = SizedBox(height: 13);

    return SingleChildScrollView
      (child:
    Column(
      crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          gap, gap,
          subject,
          gap,
          hint,
          topic,
          gap,
          meetingTimeHint,
          preferedMeetingTime,
          gap,
          text,
          gap, gap,
          addButton,
          gap
        ]
    )
    );
  }
  

}