///
//  Generated code. Do not modify.
//  source: cernlearn.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core show bool, Deprecated, double, int, List, Map, override, pragma, String;

import 'package:protobuf/protobuf.dart' as $pb;

import 'cernlearn.pbenum.dart';

export 'cernlearn.pbenum.dart';

class ServiceResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ServiceResponse', package: const $pb.PackageName('v1'))
    ..aOS(1, 'api')
    ..e<Status>(2, 'status', $pb.PbFieldType.OE, Status.ERROR, Status.valueOf, Status.values)
    ..aOS(3, 'message')
    ..hasRequiredFields = false
  ;

  ServiceResponse._() : super();
  factory ServiceResponse() => create();
  factory ServiceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ServiceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ServiceResponse clone() => ServiceResponse()..mergeFromMessage(this);
  ServiceResponse copyWith(void Function(ServiceResponse) updates) => super.copyWith((message) => updates(message as ServiceResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ServiceResponse create() => ServiceResponse._();
  ServiceResponse createEmptyInstance() => create();
  static $pb.PbList<ServiceResponse> createRepeated() => $pb.PbList<ServiceResponse>();
  static ServiceResponse getDefault() => _defaultInstance ??= create()..freeze();
  static ServiceResponse _defaultInstance;

  $core.String get api => $_getS(0, '');
  set api($core.String v) { $_setString(0, v); }
  $core.bool hasApi() => $_has(0);
  void clearApi() => clearField(1);

  Status get status => $_getN(1);
  set status(Status v) { setField(2, v); }
  $core.bool hasStatus() => $_has(1);
  void clearStatus() => clearField(2);

  $core.String get message => $_getS(2, '');
  set message($core.String v) { $_setString(2, v); }
  $core.bool hasMessage() => $_has(2);
  void clearMessage() => clearField(3);
}

class ServiceRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ServiceRequest', package: const $pb.PackageName('v1'))
    ..aOS(1, 'api')
    ..aOS(2, 'token')
    ..hasRequiredFields = false
  ;

  ServiceRequest._() : super();
  factory ServiceRequest() => create();
  factory ServiceRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ServiceRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ServiceRequest clone() => ServiceRequest()..mergeFromMessage(this);
  ServiceRequest copyWith(void Function(ServiceRequest) updates) => super.copyWith((message) => updates(message as ServiceRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ServiceRequest create() => ServiceRequest._();
  ServiceRequest createEmptyInstance() => create();
  static $pb.PbList<ServiceRequest> createRepeated() => $pb.PbList<ServiceRequest>();
  static ServiceRequest getDefault() => _defaultInstance ??= create()..freeze();
  static ServiceRequest _defaultInstance;

  $core.String get api => $_getS(0, '');
  set api($core.String v) { $_setString(0, v); }
  $core.bool hasApi() => $_has(0);
  void clearApi() => clearField(1);

  $core.String get token => $_getS(1, '');
  set token($core.String v) { $_setString(1, v); }
  $core.bool hasToken() => $_has(1);
  void clearToken() => clearField(2);
}

class AuthRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthRequest', package: const $pb.PackageName('v1'))
    ..aOS(1, 'api')
    ..aOS(2, 'code')
    ..hasRequiredFields = false
  ;

  AuthRequest._() : super();
  factory AuthRequest() => create();
  factory AuthRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AuthRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AuthRequest clone() => AuthRequest()..mergeFromMessage(this);
  AuthRequest copyWith(void Function(AuthRequest) updates) => super.copyWith((message) => updates(message as AuthRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthRequest create() => AuthRequest._();
  AuthRequest createEmptyInstance() => create();
  static $pb.PbList<AuthRequest> createRepeated() => $pb.PbList<AuthRequest>();
  static AuthRequest getDefault() => _defaultInstance ??= create()..freeze();
  static AuthRequest _defaultInstance;

  $core.String get api => $_getS(0, '');
  set api($core.String v) { $_setString(0, v); }
  $core.bool hasApi() => $_has(0);
  void clearApi() => clearField(1);

  $core.String get code => $_getS(1, '');
  set code($core.String v) { $_setString(1, v); }
  $core.bool hasCode() => $_has(1);
  void clearCode() => clearField(2);
}

class AuthResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthResponse', package: const $pb.PackageName('v1'))
    ..aOS(2, 'token')
    ..a<ServiceResponse>(3, 'response', $pb.PbFieldType.OM, ServiceResponse.getDefault, ServiceResponse.create)
    ..hasRequiredFields = false
  ;

  AuthResponse._() : super();
  factory AuthResponse() => create();
  factory AuthResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AuthResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AuthResponse clone() => AuthResponse()..mergeFromMessage(this);
  AuthResponse copyWith(void Function(AuthResponse) updates) => super.copyWith((message) => updates(message as AuthResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthResponse create() => AuthResponse._();
  AuthResponse createEmptyInstance() => create();
  static $pb.PbList<AuthResponse> createRepeated() => $pb.PbList<AuthResponse>();
  static AuthResponse getDefault() => _defaultInstance ??= create()..freeze();
  static AuthResponse _defaultInstance;

  $core.String get token => $_getS(0, '');
  set token($core.String v) { $_setString(0, v); }
  $core.bool hasToken() => $_has(0);
  void clearToken() => clearField(2);

  ServiceResponse get response => $_getN(1);
  set response(ServiceResponse v) { setField(3, v); }
  $core.bool hasResponse() => $_has(1);
  void clearResponse() => clearField(3);
}

