///
//  Generated code. Do not modify.
//  source: cernlearn.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const Status$json = const {
  '1': 'Status',
  '2': const [
    const {'1': 'ERROR', '2': 0},
    const {'1': 'OK', '2': 1},
    const {'1': 'UNAUTHORIZED', '2': 2},
  ],
};

const ServiceResponse$json = const {
  '1': 'ServiceResponse',
  '2': const [
    const {'1': 'api', '3': 1, '4': 1, '5': 9, '10': 'api'},
    const {'1': 'status', '3': 2, '4': 1, '5': 14, '6': '.v1.Status', '10': 'status'},
    const {'1': 'message', '3': 3, '4': 1, '5': 9, '10': 'message'},
  ],
};

const ServiceRequest$json = const {
  '1': 'ServiceRequest',
  '2': const [
    const {'1': 'api', '3': 1, '4': 1, '5': 9, '10': 'api'},
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
  ],
};

const AuthRequest$json = const {
  '1': 'AuthRequest',
  '2': const [
    const {'1': 'api', '3': 1, '4': 1, '5': 9, '10': 'api'},
    const {'1': 'code', '3': 2, '4': 1, '5': 9, '10': 'code'},
  ],
};

const AuthResponse$json = const {
  '1': 'AuthResponse',
  '2': const [
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'response', '3': 3, '4': 1, '5': 11, '6': '.v1.ServiceResponse', '10': 'response'},
  ],
};

