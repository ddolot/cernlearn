///
//  Generated code. Do not modify.
//  source: chat.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core show bool, Deprecated, double, int, List, Map, override, pragma, String;

import 'package:protobuf/protobuf.dart' as $pb;

class Message extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Message', package: const $pb.PackageName('v1'))
    ..aOS(1, 'pfrom')
    ..aOS(2, 'pto')
    ..aOS(3, 'text')
    ..hasRequiredFields = false
  ;

  Message._() : super();
  factory Message() => create();
  factory Message.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Message.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Message clone() => Message()..mergeFromMessage(this);
  Message copyWith(void Function(Message) updates) => super.copyWith((message) => updates(message as Message));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Message create() => Message._();
  Message createEmptyInstance() => create();
  static $pb.PbList<Message> createRepeated() => $pb.PbList<Message>();
  static Message getDefault() => _defaultInstance ??= create()..freeze();
  static Message _defaultInstance;

  $core.String get pfrom => $_getS(0, '');
  set pfrom($core.String v) { $_setString(0, v); }
  $core.bool hasPfrom() => $_has(0);
  void clearPfrom() => clearField(1);

  $core.String get pto => $_getS(1, '');
  set pto($core.String v) { $_setString(1, v); }
  $core.bool hasPto() => $_has(1);
  void clearPto() => clearField(2);

  $core.String get text => $_getS(2, '');
  set text($core.String v) { $_setString(2, v); }
  $core.bool hasText() => $_has(2);
  void clearText() => clearField(3);
}

