///
//  Generated code. Do not modify.
//  source: chat.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core show int, String, List;

import 'package:grpc/service_api.dart' as $grpc;
import 'chat.pb.dart' as $0;
import 'google/protobuf/empty.pb.dart' as $1;
import 'google/protobuf/wrappers.pb.dart' as $2;
export 'chat.pb.dart';

class ChatServiceClient extends $grpc.Client {
  static final _$sendMsg = $grpc.ClientMethod<$0.Message, $1.Empty>(
      '/v1.ChatService/SendMsg',
      ($0.Message value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$rcvMsg = $grpc.ClientMethod<$0.Message, $0.Message>(
      '/v1.ChatService/RcvMsg',
      ($0.Message value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Message.fromBuffer(value));
  static final _$registerUser = $grpc.ClientMethod<$2.StringValue, $1.Empty>(
      '/v1.ChatService/RegisterUser',
      ($2.StringValue value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$enterIssue = $grpc.ClientMethod<$2.StringValue, $1.Empty>(
      '/v1.ChatService/EnterIssue',
      ($2.StringValue value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));

  ChatServiceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$1.Empty> sendMsg($0.Message request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$sendMsg, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.Message> rcvMsg($0.Message request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$rcvMsg, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$1.Empty> registerUser($2.StringValue request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$registerUser, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$1.Empty> enterIssue($2.StringValue request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$enterIssue, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class ChatServiceBase extends $grpc.Service {
  $core.String get $name => 'v1.ChatService';

  ChatServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Message, $1.Empty>(
        'SendMsg',
        sendMsg_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Message.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Message, $0.Message>(
        'RcvMsg',
        rcvMsg_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.Message.fromBuffer(value),
        ($0.Message value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.StringValue, $1.Empty>(
        'RegisterUser',
        registerUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.StringValue.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.StringValue, $1.Empty>(
        'EnterIssue',
        enterIssue_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.StringValue.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
  }

  $async.Future<$1.Empty> sendMsg_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Message> request) async {
    return sendMsg(call, await request);
  }

  $async.Stream<$0.Message> rcvMsg_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Message> request) async* {
    yield* rcvMsg(call, await request);
  }

  $async.Future<$1.Empty> registerUser_Pre(
      $grpc.ServiceCall call, $async.Future<$2.StringValue> request) async {
    return registerUser(call, await request);
  }

  $async.Future<$1.Empty> enterIssue_Pre(
      $grpc.ServiceCall call, $async.Future<$2.StringValue> request) async {
    return enterIssue(call, await request);
  }

  $async.Future<$1.Empty> sendMsg($grpc.ServiceCall call, $0.Message request);
  $async.Stream<$0.Message> rcvMsg($grpc.ServiceCall call, $0.Message request);
  $async.Future<$1.Empty> registerUser(
      $grpc.ServiceCall call, $2.StringValue request);
  $async.Future<$1.Empty> enterIssue(
      $grpc.ServiceCall call, $2.StringValue request);
}
