///
//  Generated code. Do not modify.
//  source: cernlearn.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core show int, String, List;

import 'package:grpc/service_api.dart' as $grpc;
import 'cernlearn.pb.dart' as $3;
export 'cernlearn.pb.dart';

class LoginServiceClient extends $grpc.Client {
  static final _$loginWithCERN =
      $grpc.ClientMethod<$3.ServiceRequest, $3.ServiceResponse>(
          '/v1.LoginService/LoginWithCERN',
          ($3.ServiceRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $3.ServiceResponse.fromBuffer(value));
  static final _$loginWithToken =
      $grpc.ClientMethod<$3.AuthRequest, $3.AuthResponse>(
          '/v1.LoginService/LoginWithToken',
          ($3.AuthRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $3.AuthResponse.fromBuffer(value));

  LoginServiceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$3.ServiceResponse> loginWithCERN(
      $3.ServiceRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$loginWithCERN, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$3.AuthResponse> loginWithToken($3.AuthRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$loginWithToken, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class LoginServiceBase extends $grpc.Service {
  $core.String get $name => 'v1.LoginService';

  LoginServiceBase() {
    $addMethod($grpc.ServiceMethod<$3.ServiceRequest, $3.ServiceResponse>(
        'LoginWithCERN',
        loginWithCERN_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.ServiceRequest.fromBuffer(value),
        ($3.ServiceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.AuthRequest, $3.AuthResponse>(
        'LoginWithToken',
        loginWithToken_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.AuthRequest.fromBuffer(value),
        ($3.AuthResponse value) => value.writeToBuffer()));
  }

  $async.Future<$3.ServiceResponse> loginWithCERN_Pre(
      $grpc.ServiceCall call, $async.Future<$3.ServiceRequest> request) async {
    return loginWithCERN(call, await request);
  }

  $async.Future<$3.AuthResponse> loginWithToken_Pre(
      $grpc.ServiceCall call, $async.Future<$3.AuthRequest> request) async {
    return loginWithToken(call, await request);
  }

  $async.Future<$3.ServiceResponse> loginWithCERN(
      $grpc.ServiceCall call, $3.ServiceRequest request);
  $async.Future<$3.AuthResponse> loginWithToken(
      $grpc.ServiceCall call, $3.AuthRequest request);
}
