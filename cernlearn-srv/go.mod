module cernlearn-srv

go 1.12

require (
	github.com/go-pg/pg/v9 v9.0.0-beta.1
	github.com/golang/protobuf v1.3.2
	google.golang.org/grpc v1.22.1
)
