package grpc

import (
	"cernlearn-srv/pkg/api/v1"
	"context"
	"google.golang.org/grpc"
	"net"
	"os"
	"os/signal"
)

func RunServer(ctx context.Context, loginAPI v1.LoginServiceServer,chatAPI v1.ChatServiceServer, port string) error {
	listen, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return err
	}

	// register services
	server := grpc.NewServer()
	v1.RegisterLoginServiceServer(server, loginAPI)
	v1.RegisterChatServiceServer(server, chatAPI)

	// graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			server.GracefulStop()

			<-ctx.Done()
		}
	}()

	// start gRPC server
	return server.Serve(listen)
}