package cmd

import (
	"context"
	"flag"
	"fmt"
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"

	"cernlearn-srv/pkg/protocol/grpc"
	"cernlearn-srv/pkg/service/v1"
)

// Config is configuration for Server
type Config struct {
	GRPCPort string
	OAuthSecretKey string
	OAuthClientId string
	OAuthRedirectURL string
	DBHost string
}

// RunServer runs gRPC server and HTTP gateway
func RunServer() error {
	ctx := context.Background()

	// get configuration
	var cfg Config
	flag.StringVar(&cfg.GRPCPort, "port", "", "gRPC port to bind")
	flag.StringVar(&cfg.OAuthSecretKey, "oauth-secret", "", "Application OAuth secret key")
	flag.StringVar(&cfg.OAuthClientId, "oauth-client", "", "OAuth application ID")
	flag.StringVar(&cfg.OAuthRedirectURL, "oauth-redirect", "", "OAuth application client url")
	flag.StringVar(&cfg.DBHost, "database-host", "localhost:8080", "Database host and port")
	flag.Parse()

	db := pg.Connect(&pg.Options{
		User: "postgresql",
		Password: "postgresql",
		Database: "postgresql",
		Addr: cfg.DBHost,
	})
	defer db.Close()

	err := createSchema(db)
	if err != nil {
		panic(err)
	}

	if len(cfg.GRPCPort) == 0 {
		return fmt.Errorf("invalid TCP port for gRPC server: '%s'", cfg.GRPCPort)
	}

	loginAPI := v1.NewLoginServiceServer(cfg.OAuthSecretKey, cfg.OAuthClientId, cfg.OAuthRedirectURL, db)
	chatAPI := v1.NewChatServiceServer()

	return grpc.RunServer(ctx, loginAPI, chatAPI, cfg.GRPCPort)
}

func createSchema(db *pg.DB) error {
	for _, model := range []interface{}{(*v1.User)(nil), (*v1.Authorization)(nil)} {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			Temp: true,
			IfNotExists: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}