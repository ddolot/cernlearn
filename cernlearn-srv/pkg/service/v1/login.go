package v1

import (
	"cernlearn-srv/pkg/api/v1"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v9"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

const (
	apiVersion = "v1"
)

type loginServiceServer struct {
	secretKey string
	clientId string
	redirectURL string
	db *pg.DB
}

func NewLoginServiceServer(secretKey string, clientId string, redirectURL string, db *pg.DB) v1.LoginServiceServer {
	return &loginServiceServer{secretKey: secretKey, clientId: clientId, redirectURL: redirectURL, db: db}
}

func checkAPI(api string, current string) error {
	if len(api) > 0 && current != api {
		return status.Errorf(codes.Unimplemented,
			"unsupported API version: service implements API version '%s', but asked for '%s'", apiVersion, api)
	}
	return nil
}

//Prepare response
func prepareResponse(status v1.Status, message string) *v1.ServiceResponse {
	return &v1.ServiceResponse{
		Api: apiVersion,
		Status: status,
		Message: message,
	}
}

func (s *loginServiceServer) getToken(authCode string) (token string) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	hc := http.Client{Transport: tr}
	form := url.Values{}
	form.Add("code", authCode)
	form.Add("grant_type", "authorization_code")
	form.Add("client_secret", s.secretKey)
	form.Add("redirect_uri", s.redirectURL)
	form.Add("client_id", s.clientId)
	req, err := http.NewRequest("POST", "https://oauth.web.cern.ch/OAuth/Token", strings.NewReader(form.Encode()))
	if err != nil {
		panic(err)
	}
	req.PostForm = form
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := hc.Do(req)
	if err != nil {
		panic(err)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err.Error())
	}
	m := make(map[string]interface{})
	err = json.Unmarshal(data, &m)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(m["access_token"])

	return m["access_token"].(string)
}

func (s *loginServiceServer) getNameAndPersonID(token string) (name string, pid int) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	hc := http.Client{Transport: tr}

	req, err := http.NewRequest("GET", "https://oauthresource.web.cern.ch/api/User", nil)
	if err != nil {
		panic(err)
	}
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := hc.Do(req)
	if err != nil {
		panic(err)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err.Error())
	}
	m := make(map[string]interface{})

	err = json.Unmarshal(data, &m)
	if err != nil {
		log.Fatal(err)
	}

	return m["first_name"].(string), 0
}

func (s *loginServiceServer) LoginWithCERN(ctx context.Context, req *v1.ServiceRequest) (*v1.ServiceResponse, error) {
	// check if the API version requested by client is supported by server
	if err := checkAPI(req.Api, apiVersion); err != nil {
		return nil, err
	}

	token := s.getToken(req.Token)
	name, _ := s.getNameAndPersonID(token)

	return prepareResponse(v1.Status_OK, name), nil
}

func (s *loginServiceServer) LoginWithToken(ctx context.Context, req *v1.AuthRequest) (*v1.AuthResponse, error) {
	// check if the API version requested by client is supported by server
	if err := checkAPI(req.Api, apiVersion); err != nil {
		return nil, err
	}
	return &v1.AuthResponse{Token: "", Response: prepareResponse(v1.Status_OK, "Authorized succesfully")}, nil
}
