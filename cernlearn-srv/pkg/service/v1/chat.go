package v1

import (
	"context"
	"log"
	"reflect"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/ptypes/wrappers"

	"cernlearn-srv/pkg/api/v1"
)

// chatServiceServer is implementation of v1.ChatServiceServer proto interface
type chatServiceServer struct {
	subscribers map[string]chan v1.Message
	msg         chan string
}

func (s *chatServiceServer) EnterIssue(ctx context.Context, issue *wrappers.StringValue) (*empty.Empty, error) {
	panic("no idea")
}

func (s *chatServiceServer) RegisterUser(ctx context.Context, user *wrappers.StringValue) (*empty.Empty, error) {
	log.Printf("Requested to create user %s", user.Value)
	s.subscribers[user.Value] = make(chan v1.Message, 1000)
	return &empty.Empty{}, nil
}

// NewChatServiceServer creates Chat service object
func NewChatServiceServer() v1.ChatServiceServer {
	return &chatServiceServer{msg: make(chan string, 1000), subscribers: make(map[string]chan v1.Message)}
}

// Send sends message to the server
func (s *chatServiceServer) SendMsg(ctx context.Context, message *v1.Message) (*empty.Empty, error) {
	log.Print("New msg appeared!")
	if message != nil {
		log.Printf("Send requested: message=%v", *message)
		//s.msg <- message.Value
		//test := reflect.ValueOf(s.subscribers).MapKeys()[0]
		if message.Pto == "*" {
			for _, element := range reflect.ValueOf(s.subscribers).MapKeys() {
				if element.String() == message.Pfrom {
					continue
				}
				log.Printf("Sending to %s",element.String())
				s.subscribers[element.String()] <- *message
			}
		} else {
			s.subscribers[message.Pto] <- *message
		}
	} else {
		log.Print("Send requested: message=<empty>")
	}

	return &empty.Empty{}, nil
}

// Subscribe is streaming method to get echo messages from the server
func (s *chatServiceServer) RcvMsg(msg *v1.Message, stream v1.ChatService_RcvMsgServer) error {
	log.Print("Subscribe requested")
	Whom := msg.Pfrom
	s.subscribers[Whom] = make(chan v1.Message, 1000)
	for {
		log.Printf("I entered the loop as %s", Whom)
		m := <-s.subscribers[Whom]
		n := v1.Message{Pfrom: m.Pfrom, Text: m.Text}
		if err := stream.SendMsg(&n); err != nil {
			s.subscribers[m.Pto] <- m
			log.Printf("Stream connection failed: %v", err)
			return nil
		}
		log.Printf("Message sent: %+v", n)
	}
}
