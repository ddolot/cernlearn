package v1

import (
	"time"
)

type User struct {
	id int64
	personId int
	firstname string
}

type Authorization struct {
	id int64
	user User
	token string
}

type Issue struct{
	id int64
	user []User
	title string
	timePosted time.Time
	resolved bool
}

type Chat struct {
	id int64
	issue Issue
	user User
}

type ChatUser struct {
	chat Chat
	user User
}

type Message struct {
	id int64
	user User
	chat Chat
	time time.Time
	message string
}
