#!/bin/bash

set -e
sudo docker build -t gitlab-registry.cern.ch/ddolot/cernlearn/cernlearn-server:1.0 .
sudo docker push gitlab-registry.cern.ch/ddolot/cernlearn/cernlearn-server:1.0
helm delete --purge server
helm install --name server --namespace cernlearn cernlearn-chart/
